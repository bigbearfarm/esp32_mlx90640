/*--------------------------------------------------------------------------*/
/* mlx90640 to UECS CCM send program                                        */
/* mlx90640_2_UECS.ino                                        */
/* reference: https://github.com/sparkfun/SparkFun_MLX90640_Arduino_Example */
/* Author:     ookuma yousuke                                               */
/*                                                                          */
/* Created: 2020/08/09                                                      */ 
/* Copyright:   (c) ookuma 2020                                             */
/* Licence:     MIT License（X11 License）                                  */
/*--------------------------------------------------------------------------*/
#include <ArduinoOTA.h>
#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <Wire.h>
#include <ESPmDNS.h>
#include <SPIFFS.h>
#include <SPIFFSEditor.h>
#include "MLX90640_API.h"
#include "MLX90640_I2C_Driver.h"
#include "AsyncUDP.h"

AsyncUDP udp;

const char* ssid = "your ssid";
const char* password = "your password";

char ccm_name[] = "mlx90640";
char ccm_type[] = ".mIC";
char ccm_room[] = "1";
char ccm_region[] = "5";
char ccm_order[] = "1";
char priority[] = "1";
unsigned long interval = 10000; //uecs ccm send 10秒おきに送信
unsigned long prev, next ;

IPAddress local_IP(192, 168, 1, 110);
// Set your Gateway IP address
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);
IPAddress primaryDNS(8, 8, 8, 8);   //optional
IPAddress secondaryDNS(8, 8, 4, 4); //optional

// MLX90640
const byte MLX90640_address = 0x33; //Default 7-bit unshifted address of the MLX90640
#define TA_SHIFT 8 //Default shift for MLX90640 in open air
float mlx90640To[768];
paramsMLX90640 mlx90640;
const byte calcStart = 33; //Pin that goes high/low when calculations are complete  20200730

AsyncWebServer server(80);
AsyncWebSocket ws("/ws");

// SPIFFSEditorの認証
const char* http_username = "admin";
const char* http_password = "admin";

const char* PARAM_MESSAGE = "message";

void notFound(AsyncWebServerRequest *request) {
    request->send(404, "text/plain", "Not found");
}

void onWsEvent(AsyncWebSocket * server, AsyncWebSocketClient * client, AwsEventType type, void * arg, uint8_t *data, size_t len){
  if(type == WS_EVT_CONNECT){
    Serial.printf("ws[%s][%u] connect\n", server->url(), client->id());
  } else if(type == WS_EVT_DISCONNECT){
    Serial.printf("ws[%s][%u] disconnect\n", server->url(), client->id());
  } else if(type == WS_EVT_ERROR){
    Serial.printf("ws[%s][%u] error(%u): %s\n", server->url(), client->id(), *((uint16_t*)arg), (char*)data);
  }
}

void setUpMLX90640 () {
  if (isConnected() == false)
  {
    Serial.println("MLX90640 not detected at default I2C address. Please check wiring. Freezing.");
    while (1);
  }

  //Get device parameters - We only have to do this once
  int status;
  uint16_t eeMLX90640[832];
  status = MLX90640_DumpEE(MLX90640_address, eeMLX90640);
  if (status != 0)
    Serial.println("Failed to load system parameters");

  status = MLX90640_ExtractParameters(eeMLX90640, &mlx90640);
  if (status != 0)
    Serial.println("Parameter extraction failed");
    Serial.println(status);

  //Once params are extracted, we can release eeMLX90640 array

  //MLX90640_SetRefreshRate(MLX90640_address, 0x02); //Set rate to 2Hz
  MLX90640_SetRefreshRate(MLX90640_address, 0x03); //Set rate to 4Hz
  //MLX90640_SetRefreshRate(MLX90640_address, 0x07); //Set rate to 64Hz  
}
void setUpOTA() {
  ArduinoOTA.onStart([]() { Serial.println("Update Start"); });
  ArduinoOTA.onEnd([]() { Serial.println("Update End"); });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.println("OTA ERROR");
  });
  const char* hostName ="esp32";
  ArduinoOTA.setHostname(hostName);
  ArduinoOTA.begin();
}

void setup(){
  pinMode(calcStart, OUTPUT); //20200730
  Wire.begin();
  Wire.setClock(400000); //Increase I2C clock speed to 400kHz  20200730

  Serial.begin(115200);

  while (!Serial); //Wait for user to open terminal
  //Serial.println("MLX90640 IR Array Example");
    if (isConnected() == false)
    {
      Serial.println("MLX90640 not detected at default I2C address. Please check wiring. Freezing.");
      while (1);
    }
  //Get device parameters - We only have to do this once
  int status;
  uint16_t eeMLX90640[832];
  status = MLX90640_DumpEE(MLX90640_address, eeMLX90640);
  if (status != 0)
    Serial.println("Failed to load system parameters");

  status = MLX90640_ExtractParameters(eeMLX90640, &mlx90640);
  if (status != 0)
    Serial.println("Parameter extraction failed");
    MLX90640_SetRefreshRate(MLX90640_address, 0x04); //Set rate to 4Hz effective - Works
    Wire.setClock(1000000); //Teensy will now run I2C at 800kHz (because of clock 
    // Configures static IP address
    if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS)) {
      Serial.println("STA Failed to configure");
    }
    
  Serial.setDebugOutput(true);

  // Configures static IP address
  if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS)) {
    Serial.println("STA Failed to configure");
  }
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  if (WiFi.waitForConnectResult() != WL_CONNECTED) {
      Serial.printf("WiFi Failed!\n");
      return;
  }

  Serial.print("IP Address: ");
  Serial.println(WiFi.localIP());

  //OTA
  setUpOTA();

  SPIFFS.begin(true);

  ws.onEvent(onWsEvent);
  server.addHandler(&ws);

  // SPIFFSにあるファイルをブラウザで/editから編集できる
  server.addHandler(new SPIFFSEditor(SPIFFS,http_username,http_password));

  // SPIFFS
  server.serveStatic("/", SPIFFS, "/").setDefaultFile("index.html");

  server.onNotFound([](AsyncWebServerRequest *request){
    Serial.printf("NOT_FOUND: ");
    request->send(404);
  });
  server.begin();

  // MLX90640の初期設定
  setUpMLX90640();
  // uecs start time
  prev = 0;
}

void loop(){
  ArduinoOTA.handle();
  /*------------------------------*/
  // CAM webserver
  /*------------------------------*/
  if (ws.count() > 0) {
    // WebSocket接続中のみ温度取得する
    long startTime = millis();
    for (byte x = 0 ; x < 2 ; x++)
    {
      uint16_t mlx90640Frame[834];
      MLX90640_GetFrameData(MLX90640_address, mlx90640Frame);
      // float vdd = MLX90640_GetVdd(mlx90640Frame, &mlx90640);
      float Ta = MLX90640_GetTa(mlx90640Frame, &mlx90640);

      float tr = Ta - TA_SHIFT; //Reflected temperature based on the sensor ambient temperature
      float emissivity = 0.95;

      MLX90640_CalculateTo(mlx90640Frame, &mlx90640, emissivity, tr, mlx90640To);
    }
    long calculatedTime = millis();

    AsyncWebSocketMessageBuffer * buffer = ws.makeBuffer((uint8_t *)&mlx90640To, sizeof(mlx90640To)); 
    ws.binaryAll(buffer);   // バイナリー（uint8_tの配列）で全クライアントに送信

    long finishedTime = millis();
    Serial.printf("calculated secs:%.2f, finished secs:%.2f\n", (float)(calculatedTime - startTime)/1000, (float)(finishedTime - startTime)/1000);  
  }
  /*------------------------------*/
  // UECS brodcast
  /*------------------------------*/
  unsigned long curr = millis();    // 現在時刻を取得
  if ((curr - prev) >= interval) { //前回実行時刻から実行周期以上経過していたら  
    for (byte x = 0 ; x < 2 ; x++)
    {
      uint16_t mlx90640Frame[834];
      int status = MLX90640_GetFrameData(MLX90640_address, mlx90640Frame);

      digitalWrite(calcStart, HIGH);
      float vdd = MLX90640_GetVdd(mlx90640Frame, &mlx90640);
      float Ta = MLX90640_GetTa(mlx90640Frame, &mlx90640);

      float tr = Ta - TA_SHIFT; //Reflected temperature based on the sensor ambient temperature
      float emissivity = 0.95;

      MLX90640_CalculateTo(mlx90640Frame, &mlx90640, emissivity, tr, mlx90640To);
      digitalWrite(calcStart, LOW);
      //Calculation time on a Teensy 3.5 is 71ms
    }
    long stopReadTime = millis();
  
    for (int x = 0 ; x < 768 ; x++)
    {
      //if(x % 8 == 0) Serial.println();
      Serial.print(mlx90640To[x], 2);
      Serial.print(",");
    }
    Serial.println("");

    for (int i = 0 ; i < 3 ; i++) //(i = 0:max,1:min,2:avg)
    {
      Serial.println(get_val(mlx90640To,i));  
      float val  = get_val(mlx90640To,i);
      String name_func = get_name_func(i);

      char charBuf[6];      
      name_func.toCharArray(charBuf, 6); 

      Serial.print(name_func);      
      Serial.print(":");
      Serial.println(val);  
  
  //      const char* ccm = UECSsetCCM(val, charBuf).c_str(); not work

      String ccm_s = UECSsetCCM(val, charBuf);
      const char* ccm_c = ccm_s.c_str();

  //      Serial.print("UECSsetCCM:");
      Serial.println(ccm_c);
      udp.broadcastTo(ccm_c, 16520);

      delay(100);
    }
    prev = curr; // 前回実行時刻を現在時刻で更新
  }
}

//Returns true if the MLX90640 is detected on the I2C bus
boolean isConnected()
{
  Wire.beginTransmission((uint8_t)MLX90640_address);
  if (Wire.endTransmission() != 0)
    return (false); //Sensor did not ACK
  return (true);
}

String UECSsetCCM(float _val, char* name_func){
  
  char c_val[12]="";
  sprintf(c_val,"%f",_val);
  char ccm[512]="";
  char ccm_header[] ="<?xml version=\"1.0\"?><UECS ver=\"1.00-E10\"><DATA type=\"" ;
  char ccm_footer1[] = "</DATA><IP>" ;
  char ccm_footer2[] = "</IP></UECS> " ;
  char ccm_ip[15] = "";
  sprintf(ccm_ip, "%d.%d.%d.%d", local_IP[0], local_IP[1], local_IP[2], local_IP[3]);
  strcat(ccm,ccm_header); 
  strcat(ccm,ccm_name);
  strcat(ccm,name_func);
  strcat(ccm,ccm_type);
  strcat(ccm,"\" room=\"");
  strcat(ccm,ccm_room);
  strcat(ccm,"\" region=\"");
  strcat(ccm,ccm_region);
  strcat(ccm,"\" order=\"");  
  strcat(ccm,ccm_order);  
  strcat(ccm,"\" priority=\"");
  strcat(ccm,priority);
  strcat(ccm,"\"> ");
  strcat(ccm,c_val);
  strcat(ccm,ccm_footer1);    
  strcat(ccm,ccm_ip);
  strcat(ccm,ccm_footer2);    

  String ccm_str;
  ccm_str = ccm;
  
  return ccm_str;
}

//Get Val (func[0:Max 1:Min 2:Avg])  20200809
float get_val(float AccData[],int func){
  float val;
  int icnt =0;
  for (int i = 0 ; i < 768 ; i++){
    if (i == 0) { val = AccData[i];} 
    if (func == 0) { // get max val
      if (AccData[i]*1.0 > val ){ 
        if (AccData[i]*1.0 <= 300.0 ) { val = AccData[i]; }
      }  
    }
    if (func == 1) { // get min val
      if (AccData[i]*1.0 < val) {
        if (AccData[i]*1.0 >= -40.0) { val = AccData[i]; }
      }
    }
    if (func == 2) { // get avg val
      if (AccData[i]*1.0 <= 300.0 ) {
        if (AccData[i]*1.0 >= -40.0 ) { val += AccData[i]; icnt +=1;}
      }
    }
  }
  if (func == 2) { // get avg val
    val = val /icnt; 
    }
  return val;
}

//Get Val (func[0:Max 1:Min 2:Avg])  20200727
String get_name_func(int func){
  String name_func="";
  
  if (func==0) {
    name_func="_max";
    return name_func; }
  else if (func==1) {
    name_func="_min";
    return name_func; }
  else if (func==2) {
    name_func="_avg";
    return name_func; }

  }