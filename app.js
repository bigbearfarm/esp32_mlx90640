const ge = (s) => { return document.getElementById(s);}
const ce = (s) => { return document.createElement(s);}
const gc = (s) => { return document.getElementsByClassName(s);}
const addMessage = (m) => {
  // メッセージ表示
  // console.log(m);
  const msg = ce("div");
  msg.innerText = m;
  ge("messages").appendChild(msg);
  ge("messages").append
}
skt = {
  ws: null,
  start: function() {
    // WebSocketを開始
    ws = ws = new WebSocket('ws://'+document.location.host+'/ws',['arduino']);
    ws.binaryType = "arraybuffer";
    ws.onopen = (e) => {
      addMessage("Connected");
    };
    ws.onclose = (e) => {
      addMessage("Disconnected");
    };
    ws.onerror = (e) => {
      console.log("ws error", e);
      addMessage("Error");
    };
    ws.onmessage = (e)=>{
      if(e.data instanceof ArrayBuffer){
        // バイナリーデータの場合
        this.parseTemparatures(e.data);
      } else {
        // テキストデータの場合
        addMessage(e.data);
      }
    };
  },
  parseTemparatures: (data) => {
    // Uint8Arrayにセットされた4バイトのfloatの配列をFloat32Arrayの型付き配列にセットし、描画します。
    const dv = new DataView(data);
    const byteSize = 4;
    const tmps = new Float32Array(data.byteLength / byteSize);          
    for (let i = 0; i<tmps.length; i++) {
      tmps[i] = dv.getFloat32(i*byteSize, true);
    }
    cv.draw(tmps);          
  }
};
const HSVtoRGB = (h, s, v) => {
  // HSVからRGBに変換 パラメータh,s,vは0以上1以下
  let r, g, b, i, f, p, q, t;
  i = Math.floor(h * 6);
  f = h * 6 - i;
  p = v * (1 - s);
  q = v * (1 - f * s);
  t = v * (1 - (1 - f) * s);
  switch (i % 6) {
      case 0: r = v, g = t, b = p; break;
      case 1: r = q, g = v, b = p; break;
      case 2: r = p, g = v, b = t; break;
      case 3: r = p, g = q, b = v; break;
      case 4: r = t, g = p, b = v; break;
      case 5: r = v, g = p, b = q; break;
  }
  return {
      r: Math.round(r * 255),
      g: Math.round(g * 255),
      b: Math.round(b * 255)
  };
}

const rgb = {
    min: 20,  // 表示する最低温度
    max: 35,  // 表示する最高温度
    get: function(tmp) {
        // 温度から色（RGB）を取得
        let rate = 1 - (tmp - this.min)/(this.max - this.min);
        if (rate < 0) {
            rate = 0;
        } else if (rate > 1) {
            rate = 1;
        }
        // const h = 0.7*rate;
        const h = (Math.tanh(rate*2 - 1.5) + 1)/2 - 0.04; // 適当
        return HSVtoRGB(h, 1, 1);
    }
};

const cv = {
  canvas: null,
  content: null,
  imageData: null,
  createCanvas: function() {
    // Canvasを作成
    this.canvas = ge('canvas');
    this.context = this.canvas.getContext('2d');
    this.imageData = this.context.createImageData(32, 24);
  },
  createScale: function(){
    // スケールを作成
    const scale = ge('scale');
    let color, t, span;
    for (let i=0; i < 100; i++) {
      t = i * (rgb.max - rgb.min)/100 + rgb.min;
      span = ce('span');
      color = rgb.get(t);
      span.style.backgroundColor = span.style.color = 'rgb('+color.r+','+color.g+','+color.b+')';
      scale.appendChild(span);
    }
    this.createDivisions();
  },
  createDivisions: () => {
    // 目盛り作成
    ge("min-tmp").textContent = rgb.min;
    ge("max-tmp").textContent = rgb.max;

    var scaleDivisions = ge('scale-divisions');
    const divisions = gc('division');
    while(divisions.length > 0){
      scaleDivisions.removeChild(divisions[0]);
    }
    let div;
    for (let temp = rgb.min+5; temp < rgb.max; temp+=5) {
      div = ce('div');
      div.innerText = temp;
      div.classList.add("division");
      div.style.left = 640*(temp - rgb.min)/(rgb.max - rgb.min)-7 + 'px';
      scaleDivisions.appendChild(div);
    }
  },
  draw: function(tmps) {
    // 描画
    const data = this.imageData.data; // RGBA の順番のデータを含んだ 1次元配列。それぞれの値は 0 ～ 255 の範囲となります。
    if (data.length/4 != tmps.length) {
      alert(なにかおかしいです);
      return;
    }
    let tmp, color, j, mirror;
    const maxValue = 255;
    for (let i = 0; i < tmps.length; i++) {
      if (false) {
        j = 4 * i;
      } else {
         // 左右反転させる
        mirror = (31-i%32) + parseInt(i/32)*32;
        j = 4 * mirror;
      }
      tmp = tmps[i];
      color = rgb.get(tmp);
      data[j] = color.r;
      data[j+1] = color.g;
      data[j+2] = color.b;
      data[j+3] = maxValue;
    }
    this.context.putImageData(this.imageData, 0, 0);
  }
};

const divisionBtns = gc('divisionBtn');
for (let i=0; i < divisionBtns.length; i++){
  divisionBtns[i].addEventListener('click', function() {
    // 目盛り変更
    const d = 5; // 目盛りの間隔
    switch(this.id) {
      case 'min-down':
        if (rgb.min >= 5) rgb.min -= d;
        break;
      case 'min-up':
        if (rgb.min <= rgb.max-2*d) rgb.min += d;
        break;
        case 'max-down':
        if (rgb.min <= rgb.max-2*d) rgb.max -= d;
        break;
      case 'max-up':
        if (rgb.max <= 90) rgb.max += d;
        break;
    }
    cv.createDivisions();
  });      
}

let onBodyLoad = function(){
  cv.createScale();
  skt.start();
  cv.createCanvas();
}