 This module is depend on [lancardcom](https://www.lancard.com/blog/2019/01/28/%E8%B5%A4%E5%A4%96%E7%B7%9A%E3%82%A2%E3%83%AC%E3%82%A4%E3%82%BB%E3%83%B3%E3%82%B5%E3%83%BC%E3%80%8Cmlx90640%E3%80%8D%E3%82%92esp32%E3%81%AB%E3%81%A4%E3%81%AA%E3%81%84%E3%81%A7websocket%E3%81%97/
).
```
・index.html
・app.js
・app.css
・webserver_cam.ino
```

 another module is Copyright (c) 2020 yousuke ookuma
```
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
