# README #

UECS UDP Broadcast Program  
UECSとは、ユビキタス環境制御システム(Ubiquitous Environment Control System)の頭文字を表し、「ウエックス」と読みます。植物を生産するためのガラス室・ハウス(温室)、植物工場などの園芸施設の環境制御を実現するための優れた自律分散型システムです。  https://uecs.jp/uecs/index.html  

【Support Sensor】  
[mlx90640](https://shop.pimoroni.com/products/mlx90640-thermal-camera-breakout?variant=12536948654163)  

【Support board】  
ESP32

【Programming language】  
Arduino, ARDUINO IDE 1.8.12   


### 1.概要 ###
```
  1.サーマルカメラ 最高気温、最低気温、平均気温 を UECS CCM送信します。  
  2.WEBサーバ化によりサーマル情報を可視化します。 
```

### 2.写真・材料 ###
https://bitbucket.org/bigbearfarm/esp32_mlx90640/wiki/%E7%94%BB%E5%83%8F  
https://bitbucket.org/bigbearfarm/esp32_mlx90640/wiki/%E6%9D%90%E6%96%99  

### 3.配線図 ###
https://bitbucket.org/bigbearfarm/esp32_mlx90640/wiki/%E9%85%8D%E7%B7%9A%E5%9B%B3   

### 4.モジュール一覧  ###
```
  1.mlx90640_2_UECS.ino  ・・・main module
  2.index.html
  3.app.js
  4.app.css
  5.webserver_cam.ino   ・・・webserver module ONLY 予備であり使用しません。
  6.uecs_mlx90640.ino   ・・・uecs module ONLY 予備であり使用しません。
```

### 5.パラメータの設定 ###
ARDUINO IDE 1.8.12  
・mlx90640_2_UECS.ino    
```
26行目より
const char* ssid = "your ssid";               WIFI SSID を設定してください。  
const char* password = "your password";       WIFI password を設定してください。  

char ccm_name[] = "mlx90640";                 UECS CCM NAME を設定してください。
char ccm_type[] = ".mIC";                     UECS CCM TYPE を設定してください。
char ccm_room[] = "1";                        UECS CCM ROOM を設定してください。
char ccm_region[] = "5";                      UECS CCM REGION を設定してください。
char ccm_order[] = "1";                       UECS CCM ORDER を設定してください。
char priority[] = "1";                        UECS CCM PRIORITY を設定してください。
unsigned long interval = 10000;               10秒おきに送信
unsigned long prev, next ;

IPAddress local_IP(192, 168, 1, 110);         静的IP アドレスを設定してください。
// Set your Gateway IP address
IPAddress gateway(192, 168, 1, 1);            ルーターのGATEWAYを設定してください。

```

### 6.セットアップ  ###
#### 6.1.ESP32の開発環境を準備する ####
```
「arduino esp32 開発環境」と検索して、
・ARDUINO IDE のインストール
・Arduino Core for the ESP32 のダウンロードなど、ボードの設定作業を行ってください。
```

#### 6.2.set up 手順 ####
```
<ARDUINO IDE にて>
1.mlx90640_2_UECS.ino  を開き、ESP32 へ書き込んでください。

<ブラウザにて>
1.任意のWebサーバーへアクセスしてください。
  http://192.168.1.110/edit
2.下記のファイルをアップロードしてください。
  ・index.html
  ・app.js
  ・app.css
3.任意のWebサーバーへアクセスして動作していることを確認してください。
  http://192.168.1.110/index.html

```

#### 6.3.set up Gif アニメーション ####
	   
![l2xgeQ38haQHyq9xI7ia1596200970-1596201523.gif](https://bitbucket.org/repo/kk6XyyG/images/3779600425-l2xgeQ38haQHyq9xI7ia1596200970-1596201523.gif)

### 7.動作確認 ###

#### 7.1.Webサーバ動作確認 ####

![work.gif](https://bitbucket.org/repo/kk6XyyG/images/280592290-work.gif)

#### 7.2.UECS動作確認 ####
[UECS送受信機(Windows版のUECSパケットアナライザ)](https://uecs.org/arduino/uecsrs.html) にて正常動作を確認してください。  


### 8.注意事項 ###
  このツールを利用したことによって生じたいかなる結果についても責任を負うことはありませんので自己責任の上で使用して下さい。  

