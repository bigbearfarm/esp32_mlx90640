//
// A simple server implementation showing how to:
//  * serve static messages
//  * read GET and POST parameters
//  * handle missing pages / 404s
//
#include <ArduinoOTA.h>
#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <Wire.h>
#include <ESPmDNS.h>
#include <SPIFFS.h>
#include <SPIFFSEditor.h>
#include "MLX90640_API.h"
#include "MLX90640_I2C_Driver.h"

// MLX90640
const byte MLX90640_address = 0x33; //Default 7-bit unshifted address of the MLX90640
#define TA_SHIFT 8 //Default shift for MLX90640 in open air
float mlx90640To[768];
paramsMLX90640 mlx90640;

AsyncWebServer server(80);
AsyncWebSocket ws("/ws");

const char* ssid = "********";
const char* password = "********";
// mDNS
const char * hostName = "esp32";
// SPIFFSEditorの認証
const char* http_username = "admin";
const char* http_password = "admin";

IPAddress local_IP(192, 168, 1, 110);
// Set your Gateway IP address
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);
IPAddress primaryDNS(8, 8, 8, 8);   //optional
IPAddress secondaryDNS(8, 8, 4, 4); //optional

const char* PARAM_MESSAGE = "message";

void notFound(AsyncWebServerRequest *request) {
    request->send(404, "text/plain", "Not found");
}

void onWsEvent(AsyncWebSocket * server, AsyncWebSocketClient * client, AwsEventType type, void * arg, uint8_t *data, size_t len){
  if(type == WS_EVT_CONNECT){
    Serial.printf("ws[%s][%u] connect\n", server->url(), client->id());
  } else if(type == WS_EVT_DISCONNECT){
    Serial.printf("ws[%s][%u] disconnect\n", server->url(), client->id());
  } else if(type == WS_EVT_ERROR){
    Serial.printf("ws[%s][%u] error(%u): %s\n", server->url(), client->id(), *((uint16_t*)arg), (char*)data);
  }
}

void setUpMLX90640 () {
  if (isConnected() == false)
  {
    Serial.println("MLX90640 not detected at default I2C address. Please check wiring. Freezing.");
    while (1);
  }

  //Get device parameters - We only have to do this once
  int status;
  uint16_t eeMLX90640[832];
  status = MLX90640_DumpEE(MLX90640_address, eeMLX90640);
  if (status != 0)
    Serial.println("Failed to load system parameters");

  status = MLX90640_ExtractParameters(eeMLX90640, &mlx90640);
  if (status != 0)
    Serial.println("Parameter extraction failed");
    Serial.println(status);

  //Once params are extracted, we can release eeMLX90640 array

  //MLX90640_SetRefreshRate(MLX90640_address, 0x02); //Set rate to 2Hz
  MLX90640_SetRefreshRate(MLX90640_address, 0x03); //Set rate to 4Hz
  //MLX90640_SetRefreshRate(MLX90640_address, 0x07); //Set rate to 64Hz  
}
void setUpOTA() {
  ArduinoOTA.onStart([]() { Serial.println("Update Start"); });
  ArduinoOTA.onEnd([]() { Serial.println("Update End"); });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.println("OTA ERROR");
  });
  ArduinoOTA.setHostname(hostName);
  ArduinoOTA.begin();
}

void setup(){
  Wire.begin();
  Serial.begin(115200);
  Serial.setDebugOutput(true);

//  WiFi.begin(ssid, password);

//  while (WiFi.status() != WL_CONNECTED) {
//    delay(1000);
//    Serial.println("Connecting to WiFi..");
//  }
//  Serial.println("WiFi connected!");



    // Configures static IP address
    if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS)) {
      Serial.println("STA Failed to configure");
    }
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
    if (WiFi.waitForConnectResult() != WL_CONNECTED) {
        Serial.printf("WiFi Failed!\n");
        return;
    }

    Serial.print("IP Address: ");
    Serial.println(WiFi.localIP());


  //OTA
  setUpOTA();

  // mDNS
//  if (!MDNS.begin(hostName)) {
//      Serial.println("Error setting up MDNS responder!");
//      while(1){
//          delay(1000);
//      }
//  }

  SPIFFS.begin(true);

  ws.onEvent(onWsEvent);
  server.addHandler(&ws);

  // SPIFFSにあるファイルをブラウザで/editから編集できる
  server.addHandler(new SPIFFSEditor(SPIFFS,http_username,http_password));

  // SPIFFS
  server.serveStatic("/", SPIFFS, "/").setDefaultFile("index.htm");

  server.onNotFound([](AsyncWebServerRequest *request){
    Serial.printf("NOT_FOUND: ");
    request->send(404);
  });
  server.begin();

  // MLX90640の初期設定
  setUpMLX90640();
}

void loop(){
  ArduinoOTA.handle();

  if (ws.count() > 0) {
    // WebSocket接続中のみ温度取得する
    long startTime = millis();
    for (byte x = 0 ; x < 2 ; x++)
    {
      uint16_t mlx90640Frame[834];
      MLX90640_GetFrameData(MLX90640_address, mlx90640Frame);
      // float vdd = MLX90640_GetVdd(mlx90640Frame, &mlx90640);
      float Ta = MLX90640_GetTa(mlx90640Frame, &mlx90640);

      float tr = Ta - TA_SHIFT; //Reflected temperature based on the sensor ambient temperature
      float emissivity = 0.95;

      MLX90640_CalculateTo(mlx90640Frame, &mlx90640, emissivity, tr, mlx90640To);
    }
    long calculatedTime = millis();

    AsyncWebSocketMessageBuffer * buffer = ws.makeBuffer((uint8_t *)&mlx90640To, sizeof(mlx90640To)); 
    ws.binaryAll(buffer);   // バイナリー（uint8_tの配列）で全クライアントに送信

    long finishedTime = millis();
    Serial.printf("calculated secs:%.2f, finished secs:%.2f\n", (float)(calculatedTime - startTime)/1000, (float)(finishedTime - startTime)/1000);  
  }
}

//Returns true if the MLX90640 is detected on the I2C bus
boolean isConnected()
{
  Wire.beginTransmission((uint8_t)MLX90640_address);
  if (Wire.endTransmission() != 0)
    return (false); //Sensor did not ACK
  return (true);
}